from wialon import Wialon, WialonError
import requests
import json


def generate_url(session_id, spec, flags):
    """
    Генерирует URL-строку для запроса к API Wialon на основе переданных параметров.

    :param session_id: Сессионный идентификатор (SID), полученный в ходе авторизации
    :param spec: Спецификация поиска объектов
    :param flags: Флаги запроса
    :return: Сгенерированная URL-строка
    """

    base_url = 'https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params='
    url_params = {
        "spec": spec,
        "force": 1,
        "flags": flags,
        "from": 0,
        "to": 0
    }
    url = f"{base_url}{json.dumps(url_params)}&sid={session_id}"
    return url


def get_objects(session_id):
    """
    Возвращает URL-строку для получения списка всех объектов.

    :param session_id: Сессионный идентификатор (SID), полученный в ходе авторизации
    :return: URL-строка для запроса списка всех объектов
    """

    spec = {"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":"sys_name"}
    url = generate_url(session_id, spec, 1)
    return url


def get_location(session_id):
    """
    Возвращает URL-строку для получения списка всех объектов и их местоположения.

    :param session_id: Сессионный идентификатор (SID), полученный в ходе авторизации
    :return: URL-строка для запроса списка всех объектов и их местоположения
    """

    spec = {"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":"sys_name"}
    url = generate_url(session_id, spec, 4194305)
    return url


def get_object(session_id):
    """
    Возвращает URL-строку для поиска конкретного объекта.

    :param session_id: Сессионный идентификатор (SID), полученный в ходе авторизации
    :return: URL-строка для запроса поиска конкретного объекта
    """

    url = f'https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_item&params={{"id":26807880,"flags":8193}}&sid={session_id}'
    return url


def return_data(session_id):
    """
    Получает сессионный идентификатор (SID), передает его в URL-строку для запроса списка всех объектов и их местоположения,
    выполняет запрос и возвращает JSON-объект со списком всех объектов и их местоположением.

    :param session_id: Сессионный идентификатор (SID), полученный в ходе авторизации
    :return: JSON-объект со списком всех объектов и их местоположением
    """

    url = get_location(session_id)
    response_report = requests.get(url)
    data_report = response_report.json()
    return data_report


def request_wialon():
    """
    Создает объект Wialon, выполняет вход через токен, получает сессионный идентификатор (SID), передает его в функцию
    для получения списка всех объектов и их местоположения, выходит из аккаунта и возвращает данные.
    Обрабатывает ошибки WialonError.

    :return: JSON-объект
    """

    try:
        wialon_api = Wialon()
        result = wialon_api.token_login(token='8f4df900a3ea4384a3d765b5234e482d8BB55FA2A49FE31B6CBFA4E8898E80BE2600041B')
        wialon_api.sid = result['eid']
        data = return_data(wialon_api.sid)
        wialon_api.core_logout()
        return data
    except (WialonError, BaseException) as e:
        return  print(f"Ошибка: {e}")
