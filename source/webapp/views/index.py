from django.views.generic import TemplateView
from webapp.views.api import request_wialon


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(object_list=object_list, **kwargs)
        refueling_wialon = request_wialon()
        context['refueling_wialon'] = refueling_wialon
        return context
